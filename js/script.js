/* 
TODO:
*/

"use strict";
var remote       = require('remote');
var app          = remote.require('app');
var fs           = require('fs');
var path         = require('path');

var cfgPath      = app.getPath("userDesktop"); //userData => AppData/Roaming, userDesktop => Desktop
// var cfgPath      = app.getPath("exe");
var monthElement = document.getElementById("monthContainer");

var settings = {
    mark: "work"
};

// Beyond this point there be chaos

function getEl(name) {
    if(name[0] == ".") {
        return document.getElementsByClassName(name.substring(1))[0];
    } else if(name[0] == "#") {
        return document.getElementById(name.substring(1));
    }
}

function addWorkCounter() {
    var workDays      = document.getElementsByClassName("work").length;
    var detailsPane   = document.getElementById("detailsCount");
    detailsPane.innerHTML = workDays;
}

function readFromFile() {
    var itemDays  = document.getElementsByClassName("day");
    var savedFile = fs.readFileSync(path.join(cfgPath,  "marks.hug"), "utf-8");
    // var savedFile = fs.readFileSync(path.join(cfgPath, "../", "array.hug"), "utf-8");
    savedFile = JSON.parse(savedFile);

    for (var dates in savedFile) {
        if(savedFile.hasOwnProperty(dates)) {
            var props = savedFile[dates];
            props = props.join(" ");
            for( var i = 0; i < itemDays.length; i++) {
                if(itemDays[i].title == dates) {
                    itemDays[i].className += " " + props;
                    
                }
            }
        }
    }

    addWorkCounter();
    return;
}

function writeToFile() {
    var itemDays = document.getElementsByClassName(settings.mark);
    var classArray = [];
    var obj = {};
    for (var i = 0; i < itemDays.length; i++) {
        for (var j = 0; j < itemDays[i].classList.length; j++ ) {
             classArray[j] = itemDays[i].classList[j];
        }
        obj[itemDays[i].title] = classArray.slice(1);
    }
        var file = fs.createWriteStream(path.join(cfgPath, "marks.hug"));
        // var file = fs.createWriteStream(path.join(cfgPath, "../", "array.hug"));
        file.on('error', function(err) { console.log("There was an error with saving marks");});
        file.write(JSON.stringify(obj, null, 4)); //arr > obj
        file.end();
    return;
}

function pad(number) {
    return (number < 10 ? '0' : '') + number;
}

function range(start, stop, step) {
  var a=[start], b=start;
  while(b<stop){b+=step;a.push(b);}
  return a;
}

// moment("8-1", "MM-DD").isoWeekday(); -> 6
// weekdays = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
// var dayOfWeek = weekdays[moment(month + "-" + i, "MM-DD").isoWeekday(); + 1];
// console.log(dayOfWeek);

var currentDate      = moment().format("MMMM, MM, DD").split(", ");

function generateMonths(month) {
    var givenMonth       = moment(month, "M");
    var givenMonthArr    = givenMonth.format("MMMM, YYYY-MM").split(", ");
    var monthDiv         = document.createElement("div");
    var html             = "";
    var monthName        = givenMonthArr[0]; 
    var yearMonth        = givenMonthArr[1];
    var daysInGivenMonth = givenMonth.daysInMonth();
    var currentMonth     = currentDate[0];
    var currentDay       = currentDate[2];

    monthElement.appendChild(monthDiv);
    monthDiv.className   = 'month';
    monthDiv.id          = monthName;
    html                += "<h2>" + monthName + "</h2>";
    var fullDate;
    for(var i = 1; i <= daysInGivenMonth; i++) {
            fullDate = yearMonth + "-" + pad(i);

            // var weekdays = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
            // var dayOfWeek = weekdays[moment(month + "-" + i, "MM-DD").weekday()];
            // console.log(dayOfWeek, moment(month + "-" + i, "MM-DD").isoWeekday());

        if (i == currentDay && currentMonth == monthName) {

            html += "<div class='day current' title='"+ fullDate + " - today!" +"'>\
            <span class='day-number'>" + i + "</span><div class='icon-note hide'></div></div>";

        } else {
            html += "<div class='day' title='"+ fullDate + "'>\
            <span class='day-number'>" + i + "</span><div class='icon-note hide'></div></div>";
        }
    }
    monthDiv.innerHTML = html;
    // dayExtractor();
    return;
}

// Clears all months
function clearMonths() {
    monthElement.innerHTML = "";
    return;
}

function clearWorkDays() {
    var clearing = document.getElementsByClassName("work");
    for (var i = 0; i < clearing.length; i++) {
        clearing[i].classList.remove("work");   
    }
    addWorkCounter();
    writeToFile();
    return;
}
//     <div class="note-edit hide">
//       <div class="note-edit-inner">
//         <h3 class="note-title"></h3>
//         <p id="note-content" contentEditable="true"></p>
//       </div>
//     </div>

Element.prototype.remove = function() {
    this.parentElement.removeChild(this);
};

NodeList.prototype.remove = HTMLCollection.prototype.remove = function() {
    for(var i = this.length - 1; i >= 0; i--) {
        if(this[i] && this[i].parentElement) {
            this[i].parentElement.removeChild(this[i]);
        }
    }
};

function placeCaretAtEnd(el) {
    el.focus();
    if (typeof window.getSelection != "undefined"
            && typeof document.createRange != "undefined") {
        var range = document.createRange();
        range.selectNodeContents(el);
        range.collapse(false);
        var sel = window.getSelection();
        sel.removeAllRanges();
        sel.addRange(range);
    } else if (typeof document.body.createTextRange != "undefined") {
        var textRange = document.body.createTextRange();
        textRange.moveToElementText(el);
        textRange.collapse(false);
        textRange.select();
    }
}

var noteFile = fs.readFileSync(path.join(cfgPath, "notes.hug"), "utf-8");
var notes = JSON.parse(noteFile);

function readNote() {
    console.group("fn readNote");
    var noteDays = document.getElementsByClassName("day");
    var keys = Object.keys(notes);
    console.log(keys);
    for(var i = 0; i < keys.length; i++) {
        for(var j = 0; j < noteDays.length; j++) {
            if(keys[i] === noteDays[j].title) {
                noteDays[j].classList.add("note");
                noteDays[j].childNodes[2].classList.remove("hide");
                noteDays[j].innerHTML += '<span id="note-holder" class="hide">' + notes[keys[i]] + '</span>';
            }
        }
    }

    // for (var dates in noteFile) {

    //     if(noteFile.hasOwnProperty(dates)) {
    //         var notes = noteFile[dates];
    //         for (var i = 0; i < noteDays.length; i++ ) {
    //             if (noteDays[i].title.substring(0, 10) == dates && notes != "" && noteDays[i].childNodes.length >= 3) {
    //                 noteDays[i].classList.add("note");
    //                 noteDays[i].childNodes[2].classList.remove("hide");
    //                 noteDays[i].innerHTML += '<span id="note-holder" class="hide">' + notes + '</span>';
    //                 console.log("notes: ", dates + " " + notes);

    //                 // noteDays[i].childNodes[3].innerHTML = notes;
    //             } else {
    //                 console.log("trouble with readNOte: ", i);
    //             }
    //         }
    //     }
    // }
    console.groupEnd();
}

function writeNote(div) {
    console.log("fn writeNote");
    var noteText = getEl("#note-content");
    var content  = noteText.innerHTML;
    var title    = div.title.substring(0, 10);
    if (content != "") {
        notes[title] = content;
        div.childNodes[2].classList.remove("hide");
    }
    var file = fs.createWriteStream(path.join(cfgPath, "notes.hug"));
    file.on('error', function(err) { console.log("There was an error with saving notes");});
    file.on('finish', function () { console.log('file has been written');});
    file.write(JSON.stringify(notes, null, 4), {"flags": "a"});
    file.end();
}

var isNoteOpen = false;
function removeNote(element) {
    console.log("fn removeNote");
    writeNote(element);
    readNote();
    var el = getEl(".note-edit").remove();
    isNoteOpen = false;
}

function addNote(day) {
    console.log("fn addNote");
    if(!isNoteOpen) {
        day.classList.add("note");
        day.childNodes[2].classList.remove("hide"); // if empty, just not shows
        var noteDiv = '<div class="note-edit"><p id="note-content" contentEditable="true"></p></div>';
        day.childNodes[2].innerHTML += noteDiv;
        var note = getEl(".note-edit");
        var noteContent = getEl("#note-content");
        noteContent.focus(); console.log("focus!");

        if(day.childNodes[3] !== undefined) {
            console.log("note has content");
            noteContent.innerHTML = day.childNodes[3].innerHTML;
        }

        placeCaretAtEnd(noteContent);

        note.addEventListener("click", function(e) { //prevent click in note to add a mark
            e.stopPropagation();
        });

        document.body.addEventListener("click", function(e) {
            if(e.target && isNoteOpen){
                removeNote(day);
                readNote();
            }
            this.removeEventListener('click', arguments.callee);
        }, false);
        isNoteOpen = true;

    }
}

// function showModal(note) {
//     // readNote(); make this use note.title and read the correct date
//     var noteEdit = getEl(".note-edit");
//     var noteEditInner = getEl(".note-edit-inner");
//     var noteContent = getEl("#note-content");

//     noteEdit.classList.add("fadeInDown");
//     noteEdit.classList.remove("hide");
//     noteContent.focus();
//     window.onscroll = function() { window.scrollTo(0,0);};

//     // noteEdit.addEventListener("mousedown", function() {
//     //     hideModal(note);
//     //     console.log("removed");
//     // }, false);
// }

// function hideModal(el) {
//     console.log("hideModal");
//     var noteEdit = getEl(".note-edit");
//     noteEdit.classList.remove("fadeInDown");
//     noteEdit.classList.add("hide");
//     window.onscroll = "";
//     writeNote(el);
// }

// function readNote() {
//     return;
// }

// var notes = {};
// function writeNote(element) {
//     readNote();
//     var noteContent = getEl("#note-content").innerHTML;
//     var title = element.title;
//     var content = "noteContent.innerHTML";
//     notes[title] = content;
//     var file = fs.createWriteStream(path.join(cfgPath, "notes.hug"));
//     file.on('error', function(err) { console.log("There was an error with saving notes");});
//     file.on('finish', function () { console.log('file has been written');});
//     file.write(JSON.stringify(notes, null, 4), {"flags": "r+"});
//     file.end();
// } 

// function addNote() {
//     var noteContent = getEl("#note-content");
//     var noteTitle = getEl(".note-title");
//     noteTitle.innerHTML = this.title;      
//     this.classList.add("note");
//     this.childNodes[2].classList.remove("hide");
   
//     showModal(this);
// }
/////////////////////////////////////////////////////////////////////////
// function addNote() {
//     var noteContent = document.getElementById("note-content");
//     var noteTitle   = document.getElementsByClassName("note-title")[0];
//     var title       = this.title;

//     if(!this.classList.contains("note")) {
//         this.classList.add("note");
//         this.childNodes[2].classList.toggle("hide");
//     }

//     // function readNote() {
//     //     var noteFile = fs.readFileSync(path.join(cfgPath, "notes.hug"), "utf-8");
//     //     noteFile = JSON.parse(noteFile);
//     //     console.log(noteFile);
//     //     for (var keyNotes in noteFile) {
//     //         if(noteFile.hasOwnProperty(keyNotes)) {
//     //             var vals = noteFile[keyNotes];
//     //             console.log(vals);
//     //         }
//     //     }

//     // //     for (var dates in savedFile) {
//     // //     if(savedFile.hasOwnProperty(dates)) {
//     // //         var props = savedFile[dates];
//     // //         props = props.join(" ");
//     // //         for( var i = 0; i < itemDays.length; i++) {
//     // //             if(itemDays[i].title == dates) {
//     // //                 itemDays[i].className += " " + props;
//     // //             }
//     // //         }
//     // //     }

//     // }
//     // var notes = {};
//     // function saveNote() {
//     //     var itemNotes = document.getElementsByClassName("note")
        
//     //     if(!(noteContent.innerHTML === "")) {
//     //         for (var i = 0; i < itemNotes.length; i++) {
//     //             console.log("these many notes: " + i); 
//     //         }
//     //     }

//     //         var title = noteTitle.innerHTML;
//     //         var content = noteContent.innerHTML;
//     //         notes[title] = content;

//     //         var file = fs.createWriteStream(path.join(cfgPath, "notes.hug"));
//     //         file.on('error', function(err) { console.log("There was an error with saving notes");});
//     //         file.write(JSON.stringify(notes, null, 4));
//     //         file.end();
//     // }

//     function removeNote() {
//     }
    
//     function createModal() {
//         var noteEdit      = document.getElementsByClassName("note-edit")[0];
//         var noteEditInner = document.getElementsByClassName("note-edit-inner")[0];
//         var saveButton    = document.getElementById("note-save-button");
//         window.onscroll = function() { window.scrollTo(0,0);};
//         noteEdit.classList.remove("hide");
//         noteEdit.classList.add("fadeInDown");
//         noteEditInner.id = title + "-note";
//         noteTitle.innerHTML = title;
//         noteContent.focus();
//         var modalExist = false;
//         // readNote();

//         function removeModal() {
//             window.onscroll = "";
//             noteEditInner.id = "";
//             noteContent.innerHTML = "";
//             noteEdit.classList.add("hide");
//             modalExist = true;
//             console.log("close");
//             if (!modalExist) {
//                 noteEdit.removeEventListener("mousedown", removeModal());
//             }
//         }
        
//         //Add eventlistener to remove note
//         noteEdit.addEventListener("mousedown", removeModal(), false);

//         // saveButton.addEventListener("mousedown", function () {
//         //     saveNote();
//         //     console.log("save note!");
//         // }, true);

//         //Eventlistener to prevent click in child removing note
//         noteEditInner.addEventListener("mousedown", function (e) {
//             // if(!(e.target.nodeName === "SPAN")) {
//                 e.stopPropagation();
//             // };
//         });
//     } createModal();
// }

function addMark() { // make a setting for what to mark days, color etc
    this.classList.toggle(settings.mark);
    addWorkCounter();
    writeToFile();
}

function dayExtractor() {
    var days = document.getElementsByClassName("day");
    var totalDays = days.length;
    for(var i = 0; i < totalDays; i++) {
        days[i].addEventListener("click", addMark, false);
        days[i].addEventListener("contextmenu", function(e) {
            if (e.target.nodeName == "SPAN") {
                addNote(e.target.parentElement); 
                readNote();
            } else {
                addNote(e.target);
                readNote();
            }
            
        }, false);
    }
    return;
}


function monthSelector(from, to) { //speed up if possible
        from = +from;
        to = +to;
        var selectedMonths = range(from, to, 1);
        clearMonths();
        for (var i = 1; i <= selectedMonths.length; i++) {
            generateMonths(selectedMonths[i - 1]);
        }
        dayExtractor();
}

function makeSlider(){
    var slider = document.getElementById('slider');
    // var thisMonth = +moment().format("MM");
    var thisMonth        = +currentDate[1];

    noUiSlider.create(slider, {
        start: [thisMonth - 1, thisMonth + 1],
        connect: true,
        snap: true,
        step:1,
        behaviour: 'drag-tap',
        animate: true,
        range: {
            'min': 1,                // Jan
            '9.09090909090909%': 2,  // Feb
            '18.1818181818182%': 3,  // Mar
            '27.2727272727273%': 4,  // Apr
            '36.3636363636364%': 5,  // May
            '45.4545454545455%': 6,  // Jun
            '54.5454545454545%': 7,  // Jul
            '63.6363636363636%': 8,  // Aug
            '72.7272727272727%': 9,  // Sep
            '81.8181818181818%': 10, // Oct
            '90.9090909090909%': 11, // Nov
            'max': 12                // Dec
        },
        pips: {
            mode: 'values',
            values: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
            density: 9
        }
    });

    var snapValues = [
        document.getElementById('slider-snap-value-lower'),
        document.getElementById('slider-snap-value-upper')
    ];

    var shortMonthList = moment.monthsShort();
    var monthList      = moment.months();

    var pipVals = document.getElementsByClassName("noUi-value");
    for(var i = 0; i < pipVals.length; i++) {
        pipVals[i].innerHTML = shortMonthList[i];
    }

    slider.noUiSlider.on('update', function( values, handle ) {
        var newNum = +values[handle];
        snapValues[handle].innerHTML = monthList[newNum - 1];
    });

    slider.noUiSlider.on('change', function( values, handle ) {
        monthSelector(+values[0], +values[1]);
        readFromFile();
        readNote();
    });

    slider.noUiSlider.on('slide', function( values, handle ) {
        monthSelector(+values[0], +values[1]);
        readFromFile();
        readNote();
    });
    monthSelector(thisMonth - 1, thisMonth + 1);
    readFromFile();
    readNote();
}
makeSlider();

function menuShow() {
    var button = document.getElementById("menu-button");
    var list = document.getElementById("menu-list-id");
    function toggleClass() {
        list.classList.toggle("hide");
        list.classList.toggle("fadeInDown");
    }
    button.addEventListener("click", toggleClass, false);
}
menuShow();

